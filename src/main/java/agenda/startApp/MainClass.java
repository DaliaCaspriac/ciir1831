package agenda.startApp;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

import agenda.controller.ControllerApp;
import agenda.exceptions.InvalidFormatException;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.model.repository.interfaces.RepositoryUser;
import com.sun.org.apache.bcel.internal.classfile.SourceFile;

//functionalitati
//i.	 adaugarea de contacte (nume, adresa, numar de telefon, adresa email);
//ii.	 programarea unor activitati (denumire, descriere, data, locul, ora inceput, durata, contacte).
//iii.	 generarea unui raport cu activitatile pe care le are utilizatorul (nume, user, parola) la o anumita data, ordonate dupa ora de inceput.

public class MainClass {

	private static ControllerApp controllerApp;

	public static void main(String[] args) {
		BufferedReader in = null;
		try {
//			RepositoryContact contactRep = new RepositoryContactFile();
//			RepositoryUser userRep = new RepositoryUserFile();
//			RepositoryActivity activityRep = new RepositoryActivityFile(
//					contactRep);
			controllerApp = new ControllerApp();

			User user = null;
			in = new BufferedReader(new InputStreamReader(System.in));
			while (user == null) {
				System.out.printf("Enter username: ");
				String u = in.readLine();
				System.out.printf("Enter password: ");
				String p = in.readLine();

				user = controllerApp.getByUsername(u);
				if (!user.isPassword(p))
					user = null;
			}

			int chosen = 0;
			while (chosen != 4) {
				printMenu();
				chosen = Integer.parseInt(in.readLine());
				try {
					switch (chosen) {
					case 1:
						adaugContact(controllerApp, in);
						break;
					case 2:
						adaugActivitate(controllerApp, in, user);
						break;
					case 3:
						afisActivitate(controllerApp, in, user);
						break;
					}
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
			// List<Activity> act =
			// activityRep.activitiesByName(user.getName());
			// for(Activity a : act)
			// System.out.println(a.toString());

		} catch (Exception e) {

		}
		System.out.println("Program over and out\n");
	}

	private static void afisActivitate(ControllerApp controllerApp,
			BufferedReader in, User user) {
		try {
			System.out.printf("Afisare Activitate: \n");
			System.out.printf("Data(format: mm/dd/yyyy): ");
			String dateS = in.readLine();
			Calendar c = Calendar.getInstance();
			c.set(Integer.parseInt(dateS.split("/")[2]),
					Integer.parseInt(dateS.split("/")[0]) - 1,
					Integer.parseInt(dateS.split("/")[1]));
			Date d = c.getTime();

			System.out.println("Activitatile din ziua " + d.toString() + ": ");
			System.out.println(" ");

			List<Activity> act = controllerApp.activitiesOnDate(user.getName(), d);
			//System.out.println("CEVA" + act.size());
			for (Activity a : act) {
//				System.out.printf("%s - %s: %s - %s with: ", a.getStart()
//						.toString(), a.getDuration().toString(), a
//						.getDescription());
				System.out.println(a.getName() + " " + a.getStart() + " " + a.getLocation());
				for (Contact con : a.getContacts())
					System.out.printf("%s, ", con.getName());
				System.out.println();
			}
		} catch (IOException e) {
			System.out.printf("Eroare de citire: %s\n" + e.getMessage());
		}
	}

	private static void adaugActivitate(ControllerApp controllerApp,
			BufferedReader in, User user) {
		try {
			System.out.printf("Adauga Activitate: \n");
			System.out.printf("Denumire: ");
			String name = in.readLine();
			System.out.printf("Descriere: ");
			String description = in.readLine();
			System.out.printf("Start Date(format: mm/dd/yyyy): ");
			String dateS = in.readLine();
			System.out.printf("Loc: ");
			String location = in.readLine();
			System.out.printf("Start Time(hh:mm): ");
			String timeS = in.readLine();
//			System.out.printf("Durata: ");
//			String duration = in.readLine();
//			int min = Integer.parseInt(duration);
			Calendar c = Calendar.getInstance();
			c.set(Integer.parseInt(dateS.split("/")[2]),
					Integer.parseInt(dateS.split("/")[0]) - 1,
					Integer.parseInt(dateS.split("/")[1]),
					Integer.parseInt(timeS.split(":")[0]),
					Integer.parseInt(timeS.split(":")[1]));
			Date start = c.getTime();

			System.out.printf("End Date(format: mm/dd/yyyy): ");
			String dateE = in.readLine();
			System.out.printf("End Time(hh:mm): ");
			String timeE = in.readLine();

			c.set(Integer.parseInt(dateE.split("/")[2]),
					Integer.parseInt(dateE.split("/")[0]) - 1,
					Integer.parseInt(dateE.split("/")[1]),
					Integer.parseInt(timeE.split(":")[0]),
					Integer.parseInt(timeE.split(":")[1]));
			Date end = c.getTime();

			Contact c1 = controllerApp.getContacts().get(0);
			Contact c2 = controllerApp.getContacts().get(2);
			List<Contact> contacts = new ArrayList<Contact>();
			contacts.add(c1);
			contacts.add(c2);
			Activity act = new Activity(user.getName(),name, description, location, start, end, contacts);

			controllerApp.addActivity(act);

			System.out.printf("S-a adaugat cu succes\n");
		} catch (IOException e) {
			System.out.printf("Eroare de citire: %s\n" + e.getMessage());
		}

	}

	private static List<Contact> generateRandomContacts() {
		List<Contact> contacts = new ArrayList<Contact>();
		Random rand = new Random();
		int max = controllerApp.getContacts().size();
		//System.out.println("LUNGIME" + max);
		int min = 0;
		int randomNumber = rand.nextInt( max- min + 1) + min;
		List<Contact> existedContacts = controllerApp.getContacts();

		for(int i = 0; i < randomNumber; i++) {
			randomNumber = rand.nextInt( max- min + 1) + min;
			Contact c = existedContacts.get(randomNumber);
			contacts.add(c);
		}

		return contacts;
	}
	private static void adaugContact(ControllerApp controllerApp,
			BufferedReader in) {

		try {
			System.out.printf("Adauga Contact: \n");
			System.out.printf("Nume: ");
			String name = in.readLine();
			System.out.printf("Adresa: ");
			String adress = in.readLine();
			System.out.printf("Numar de telefon: ");
			String telefon = in.readLine();
			System.out.printf("Email: ");
			String email = in.readLine();
			
			Contact c = new Contact(name, adress, telefon,email);

			controllerApp.addContact(c);

			System.out.printf("S-a adugat cu succes\n");
		} catch (IOException e) {
			System.out.printf("Eroare de citire: %s\n" + e.getMessage());
		} catch (InvalidFormatException e) {
			if (e.getCause() != null)
				System.out.printf("Eroare: %s - %s\n" + e.getMessage(), e
						.getCause().getMessage());
			else
				System.out.printf("Eroare: %s\n" + e.getMessage());
		}

	}

	private static void printMenu() {
		System.out.printf("Please choose option:\n");
		System.out.printf("1. Adauga contact\n");
		System.out.printf("2. Adauga activitate\n");
		System.out.printf("3. Afisare activitati din data de...\n");
		System.out.printf("4. Exit\n");
		System.out.printf("Alege: ");
	}
}
