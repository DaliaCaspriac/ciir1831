package agenda.controller;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.base.User;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.classes.RepositoryUserFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import agenda.model.repository.interfaces.RepositoryUser;

import java.util.Date;
import java.util.List;

public class ControllerApp {
    private RepositoryContact contactRep;
    private RepositoryUser userRep;
    private RepositoryActivity activityRep;

    public ControllerApp(){
        try {
            contactRep = new RepositoryContactFile();
            userRep = new RepositoryUserFile();
            activityRep = new RepositoryActivityFile(
                    contactRep);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public User getByUsername(String username) {
        return userRep.getByUsername(username);
    }

    public void addContact(Contact contact) {
        contactRep.addContact(contact);
    }

    public void addActivity(Activity activity) {
        activityRep.addActivity(activity);
    }

    public List<Activity> activitiesOnDate(String name, Date d) {
        return activityRep.activitiesOnDate(name, d);
    }

    public List<Contact> getContacts(){
        return contactRep.getContacts();
    }
}
