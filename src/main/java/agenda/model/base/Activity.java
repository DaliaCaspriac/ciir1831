package agenda.model.base;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import agenda.model.repository.interfaces.RepositoryContact;

public class Activity {
	private String user;
	private String name;
	private String description;
	private String location;
	private Date start;
	private Date duration;
	private List<Contact> contacts;

	public Activity(String user,String name,String description,String location, Date start, Date end, List<Contact> contacts) {
		this.user = user;
		this.name = name;
		this.location = location;
		this.description = description;
		if (contacts == null)
			this.contacts = new LinkedList<Contact>();
		else
			this.contacts = new LinkedList<Contact>(contacts);

		this.start = new Date();
		this.start.setTime(start.getTime());
		this.duration = new Date();
		this.duration.setTime(end.getTime());
	}

	public String getName() {
		return name;
	}

	public Date getStart() {
		return start;
	}

	public void setStart(Date start) {
		this.start = start;
	}

	public Date getDuration() {
		return duration;
	}

	public void setDuration(Date duration) {
		this.duration = duration;
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}

	public String getDescription() {
		return description;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Activity))
			return false;
		Activity act = (Activity) obj;
		if (act.description.equals(description) && start.equals(act.start)
				&& duration.equals(act.duration) && name.equals(act.name))
			return true;
		return false;
	}

	public boolean intersect(Activity act) {
		if (start.compareTo(act.duration) < 0
				&& act.start.compareTo(duration) < 0)
			return true;
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(user);
		sb.append("#");
		sb.append(name);
		sb.append("#");
		sb.append(description);
		sb.append("#");
		sb.append(location);
		sb.append("#");
		sb.append(start.getTime());
		sb.append("#");
		sb.append(duration.getTime());
		//sb.append("#");
		for (Contact c : contacts) {
			sb.append("#");
			sb.append(c.getName());
		}
		return sb.toString();
	}

	public static Activity fromString(String line, RepositoryContact repcon) {
		try {
			String[] str = line.split("#");
			String user = str[0];
			String name = str[1];
			String description = str[2];
			String location = str[3];
			Date start = new Date(Long.parseLong(str[4]));
			Date duration = new Date(Long.parseLong(str[5]));

			List<Contact> conts = new LinkedList<Contact>();
			for (int i = 6; i < str.length; i++) {
				conts.add(repcon.getByName(str[i]));
			}
			return new Activity(user,name,description,location, start, duration, conts);
		} catch (Exception e) {
			return null;
		}
	}
}
