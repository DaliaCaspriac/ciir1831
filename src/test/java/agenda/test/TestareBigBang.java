package agenda.test;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestareBigBang {
    private RepositoryContact repositoryContact;
    private RepositoryActivity repositoryActivity;

    @Before
    public void setup() throws Exception{
        repositoryContact = new RepositoryContactFile();
        repositoryActivity = new RepositoryActivityFile(repositoryContact);
    }

    @Test
    public void addContact() throws Exception{
        int currentLen = repositoryContact.getContacts().size();
        Contact contact = new Contact("eu","addresss","0729919839","eu@gmail.com");
        repositoryContact.addContact(contact);

        assertEquals(currentLen + 1,repositoryContact.getContacts().size());
    }

    @Test
    public void addActivity() throws Exception{
        Contact c1 = new Contact("name12", "adresa", "0752283212", "em1@gmail.com");
        Contact c2 = new Contact("name22", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c1);
        contacts.add(c2);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("username1","myactivity","fun","cluj",df.parse("10/10/2017 12:00"),df.parse("10/13/2017 14:00"),contacts);
        boolean res = repositoryActivity.addActivity(activity);
        assertEquals(res,true);
    }

    @Test
    public void activityOnDate() throws Exception {
        Contact c1 = new Contact("name12", "adresa", "0752283212", "em1@gmail.com");
        Contact c2 = new Contact("name22", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c1);
        contacts.add(c2);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("username1", "myactivity2", "fun", "cluj", df.parse("11/10/2017 12:00"), df.parse("11/12/2017 14:00"), contacts);
        repositoryActivity.addActivity(activity);
        List<Activity> activities = repositoryActivity.activitiesOnDate("username1",df.parse("10/10/2017 12:00"));
        assertEquals(1,activities.size());
    }

    @Test
    public void integrareBigBang() throws Exception{
        addContact();
        addActivity();
        activityOnDate();
    }


}
