package agenda.test;

import agenda.exceptions.InvalidFormatException;
import agenda.model.base.Contact;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ContactFieldsTest {
    //private InvalidFormatException formatException;
    private Contact contact;

    @Before
    public void setUp(){
        try{
            contact = new Contact("Popescu","Bucuresti","+0752283212","email@yahoo.com");
        }catch(InvalidFormatException ex){
            System.out.println("Cannot create this object!");
        }
    }

    @Test(expected = InvalidFormatException.class)
    public void testInvalidPhoneNumber() throws InvalidFormatException{
        // <10
        contact.setTelefon("075228321");

    }

    @Test(expected = InvalidFormatException.class)
    public void testInvalidPhoneNumber2() throws InvalidFormatException{
        // >10
        contact.setTelefon("07522832120");
    }

    @Test(expected = InvalidFormatException.class)
    public void testInvalidPhoneNumber3() throws InvalidFormatException{
        //nu incepe cu 0 sau +
        contact.setTelefon("1234567890");
    }

    @Test(expected = InvalidFormatException.class)
    public void testInvalidPhoneNumber4() throws InvalidFormatException{
        contact.setTelefon("07522+83212");
    }

    @Test
    public void testValidPhoneNumber() throws InvalidFormatException{
        contact.setTelefon("0752283219");
        assertEquals(contact.getTelefon(),"0752283219");
    }

    @Test
    public void testValidPhoneNumber6() throws InvalidFormatException{
        contact.setTelefon("+0752283219");
        assertEquals(contact.getTelefon(),"+0752283219");
    }

    @Test
    public void testValidEmail() throws InvalidFormatException{
        contact.setEmail("dalia@yahoo.ro");
        assertEquals(contact.getEmail(),"dalia@yahoo.ro");
    }

    @Test(expected = InvalidFormatException.class)
    public void testInvalidEmail() throws InvalidFormatException{
        contact.setEmail("@mail.y");
    }

    @Test(expected = InvalidFormatException.class)
    public void testInvalidEmail9() throws InvalidFormatException{
        contact.setEmail("123yahoo.com");
    }


    @Test(expected = InvalidFormatException.class)
    public void testInvalidEmail10() throws InvalidFormatException{
        contact.setEmail("mail@yahoo.");
    }
}
