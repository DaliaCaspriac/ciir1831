package agenda.test;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class RepositoryActivityTest {
    private RepositoryActivity repositoryActivity;
    private RepositoryContact repositoryContact;

    @Before
    public void setUp() throws Exception {
        repositoryContact = new RepositoryContactFile();
        repositoryActivity = new RepositoryActivityFile(repositoryContact);
    }

    @Test
    public void addActivityValid() throws Exception {
        Contact c1 = new Contact("name", "adresa", "0752283212", "em1@gmail.com");
        Contact c2 = new Contact("name2", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c1);
        contacts.add(c2);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("04/20/2013 12:00"),
                df.parse("04/25/2013 14:00"), contacts);
        boolean result = repositoryActivity.addActivity(act);

        assertEquals(result, true);
        repositoryActivity.removeActivity(act);
        repositoryContact.removeContact(c1);
        repositoryContact.removeContact(c2);
    }

    @Test
    public void addActivityValid2() throws Exception {
        Contact c10 = new Contact("name", "adresa", "0752283212", "em1@gmail.com");
        Contact c20 = new Contact("name2", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c10);
        contacts.add(c20);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username2", "act1", "fun", "Madrid", df.parse("04/02/2013 12:00"),
                df.parse("04/04/2013 14:00"), contacts);
        boolean result = repositoryActivity.addActivity(act);

        assertEquals(result, true);
        repositoryActivity.removeActivity(act);
        repositoryContact.removeContact(c10);
        repositoryContact.removeContact(c20);
    }

    @Test
    public void addActivityInvalid() throws Exception {
        Contact c1 = new Contact("name", "adresa", "0752283212", "em1@gmail.com");
        Contact c2 = new Contact("name2", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c1);
        contacts.add(c2);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("05/22/2013 12:00"),
                df.parse("05/28/2013 14:00"), contacts);

        Activity act1 = new Activity("username1", "act1", "fun", "Madrid", df.parse("05/23/2013 12:00"),
                df.parse("05/25/2013 14:00"), contacts);
        repositoryActivity.addActivity(act);
        boolean result = repositoryActivity.addActivity(act1);
        assertEquals(result, false);

        repositoryActivity.removeActivity(act);
        repositoryActivity.removeActivity(act1);
        repositoryContact.removeContact(c1);
        repositoryContact.removeContact(c2);
    }

    @Test
    public void addActivityInvalid2() throws Exception {
        Contact c1 = new Contact("name12", "adresa", "0752283212", "em1@gmail.com");
        Contact c2 = new Contact("name22", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c1);
        contacts.add(c2);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity act = new Activity("username1", "act1", "fun", "Madrid", df.parse("05/22/2013 12:00"),
                df.parse("05/28/2013 14:00"), contacts);

        Activity act1 = new Activity("username1", "act1", "fun", "Madrid", df.parse("05/29/2013 12:00"),
                df.parse("05/30/2013 14:00"), contacts);
        repositoryActivity.addActivity(act);
        boolean result = repositoryActivity.addActivity(act1);
        assertEquals(result, true);

        repositoryActivity.removeActivity(act);
        repositoryActivity.removeActivity(act1);
        repositoryContact.removeContact(c1);
        repositoryContact.removeContact(c2);
    }
}
