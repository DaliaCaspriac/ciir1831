package agenda.test;

import agenda.model.base.Activity;
import agenda.model.base.Contact;
import agenda.model.repository.classes.RepositoryActivityFile;
import agenda.model.repository.classes.RepositoryContactFile;
import agenda.model.repository.interfaces.RepositoryActivity;
import agenda.model.repository.interfaces.RepositoryContact;
import org.junit.Before;
import org.junit.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class TestareTopDown {
    private RepositoryContact repositoryContact;
    private RepositoryActivity repositoryActivity;

    @Before
    public void setup() throws Exception {
        repositoryContact = new RepositoryContactFile();
        repositoryActivity = new RepositoryActivityFile(repositoryContact);
    }

    @Test
    public void addContact() throws Exception {
        int currentLen = repositoryContact.getContacts().size();
        Contact contact = new Contact("eu", "addresss", "0729919839", "eu@gmail.com");
        repositoryContact.addContact(contact);

        assertEquals(currentLen + 1, repositoryContact.getContacts().size());
    }

    @Test
    public void integrareB() throws Exception {
        addContact();
        Contact c1 = new Contact("name12", "adresa", "0752283212", "em1@gmail.com");
        Contact c2 = new Contact("name22", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c1);
        contacts.add(c2);
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("username1", "myactivity3", "fun", "barcelona", df.parse("09/29/2017 12:00"), df.parse("09/30/2017 14:00"), contacts);
        boolean res = repositoryActivity.addActivity(activity);
        assertEquals(res, true);
    }

    @Test
    public void integrareC() throws Exception {
        addContact();

        Contact c1 = new Contact("name12", "adresa", "0752283212", "em1@gmail.com");
        Contact c2 = new Contact("name22", "adresa", "0753384919", "email@yahoo.ro");
        List<Contact> contacts = new ArrayList<Contact>();
        contacts.add(c1);
        contacts.add(c2);

        DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        Activity activity = new Activity("username1", "newactvity", "bussines", "roma", df.parse("12/12/2016 13:00"), df.parse("12/15/2016 18:00"), contacts);
        boolean res = repositoryActivity.addActivity(activity);
        assertEquals(res, true);

        List<Activity> activities = repositoryActivity.activitiesOnDate("username1",df.parse("12/12/2016 13:00"));
        assertEquals(1,activities.size());


    }


}
